Internal flashing is possible on this IdeaPad (20150). I followed the instructions at https://github.com/gch1p/thinkpad-bios-software-flashing-guide

DO NOT FLASH unless you have backed up the 2 flash chips with an external programmer! DO NOT ATTEMPT at reading the whole region internally until you have made backups of all regions (IFD, GBE, ME, BIOS).

TO DO
- [ ] "If autoport says assuming removable then you're fine. If it doesn't then you may want to add relevant PCIIDs to autoport. When rerunning you can skip argument --make_logs to reuse the same logs."

- [x] Identify flash chips
    - Winbond 25Q32BVSIG           [U5]
    - cFeon   QH16-104HIP   127xB2 [U6]
